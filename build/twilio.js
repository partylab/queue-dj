
const _ = require('lodash')
const twilio = require('twilio')
const fb = require('./firebase')

const config = {
  accountSid: 'AC55ae631cf12b22c6abeb2a41984fc5f1',
  authToken: 'bcea3a8c16539d99b0a2e993631b2497',
  sendingNumber: '+18659990909'
}

// Initialize the Firebase connection
const client = twilio(config.accountSid, config.authToken)

const actions = {
  send ({to, message}) {
    return new Promise((resolve, reject) => {
      client.messages.create({
        body: message,
        to: to,
        from: config.sendingNumber
      }, (err, data) => {
        if (err) {
          console.error('Could not notify user')
          reject(err)
        } else {
          console.log('User notified')
          resolve()
        }
      })
    })
  },
  sendList ({room, ids}, message) {
    let remaining, sent;
    return new Promise((resolve, reject) => {
      fb.db.ref(`room/${room}/notifications`)
        .once('value')
        .then((snapshot) => {
          let promises = []

          remaining = snapshot.val()
          sent = _.pick(remaining, ids)

          _.values(sent).forEach(to => {
            promises.push(
              actions.send({
                to,
                message
              })
            )
          })

          Promise.all(promises).then(resolve).catch(reject)

          remaining = _.omit(remaining, Object.keys(sent))
          fb.db.ref(`room/${room}/notifications`).set(remaining)
      })
    })
  }
}

const api = {
  // POST
  next (data) {
    const message = 'Your song is up next!'
    return actions.sendList(data, message)
  },
  accepted (data) {
    const message = 'Your request has been added to the queue!'
    return actions.sendList(data, message)
  }
}

module.exports = api
