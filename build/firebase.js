const admin = require("firebase-admin")
const serviceAccount = require('./serviceAccountKey.json')

// Initialize the Firebase connection
const app = admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://queue-dj.firebaseio.com"
});
const db = app.database()

module.exports = {
  db
}