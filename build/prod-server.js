var config = require('../config')
var path = require('path')
var express = require('express')
var bodyParser = require('body-parser')
var twilio = require('./twilio')

// default port where dev server listens for incoming traffic
var port = process.env.PORT || config.build.port
var host = process.env.HOST || config.build.host

var app = express()
app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(bodyParser.json());

// API Routes
app.get('/api/sms/:action', function(req, res) {
  const action = twilio[req.params.action.toLowerCase()]
  action(req.query)
  .then(() => {
    res.status(200).send(`
      Successfully called API action ${req.params.action}
    `)
  })
  .catch((err) => {
    res.status(500).send(`
      Failed to call API action ${req.params.action}: ${err}
    `)
  })
})

app.post('/api/sms/:action', function(req, res) {
  const action = twilio[req.params.action.toLowerCase()]
  action(req.body)
  .then(() => {
    res.status(200).send(`
      Successfully called API action ${req.params.action}
    `)
  })
  .catch((err) => {
    res.status(500).send(`
      Failed to call API action ${req.params.action}: ${err}
    `)
  })
})

// handle fallback for HTML5 history API
app.use(require('connect-history-api-fallback')())

// serve pure static assets
var staticPath = path.posix.join(config.build.assetsPublicPath, config.build.assetsSubDirectory)
app.use(staticPath, express.static(path.resolve(__dirname, '..', 'public/static')))

app.get('*', function response(req, res) {
  res.sendFile(path.resolve(__dirname, '..', 'public/index.html'));
});

module.exports = app.listen(port, host, function (err) {
  if (err) {
    console.log(err)
    return
  }
})
