# queue.dj

[![build status](https://gitlab.com/partylab/queue-dj/badges/master/build.svg)](https://gitlab.com/partylab/queue-dj/commits/master)
[![coverage report](https://gitlab.com/partylab/queue-dj/badges/master/coverage.svg?job=unit)](https://gitlab.com/partylab/queue-dj/commits/master)

## Website

Visit [queue.dj](https://queue.dj) to get the party started 🎧

## Installation

```bash
git clone https://gitlab.com/partylab/queue-dj.git
cd queue-dj

USING NPM
npm install
npm run dev

USING YARN
yarn install
yarn run dev

```

The browser should automatically navigate to the application at [http://localhost:8080](http://localhost:8080)

## Install a development environment

- Up-to-date version of Node.js - we recommend using [nvm](https://github.com/creationix/nvm)

## Software stack

queue.dj is a client-side [Vue.js](https://vuejs.org/) application. The following is required to develope this project:

- Node.js 7.0.0+
- npm 4.0.0+

## Documentation

All documentation can be found in the [wiki](wikis/home).

## Style Guides

Please adhere to the [JavaScript](https://github.com/feross/standard/blob/master/RULES.md#javascript-standard-style) and [UX](wiki/home) style guides when creating designs and implementing code.

## Build Setup

For detailed explanation on how things work, checkout the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# run unit tests
npm run unit

# run e2e tests
npm run e2e

# run all tests
npm test
```
