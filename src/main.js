/* eslint-disable no-new, no-unused-vars */

import Vue from 'vue'
import VueFire from 'vuefire'
import VueRouter from 'vue-router'

import router from './routes'
import Event from './event'
import VueAnalytics from 'vue-analytics'

window.Event = new Event()

Vue.use(VueFire)
Vue.use(VueRouter)
Vue.use(VueAnalytics, {
  id: 'UA-89270700-2',
  router
})

new Vue({
  el: '.app',
  template: '<router-view></router-view>',
  router
})
