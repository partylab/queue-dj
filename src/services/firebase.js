
import Firebase from 'firebase'

const config = {
  apiKey: 'AIzaSyBCS1_TkSAJFES61otmz6pLQGJuM8mjZ6w',
  authDomain: 'queue-dj.firebaseapp.com',
  databaseURL: 'https://queue-dj.firebaseio.com',
  storageBucket: 'queue-dj.appspot.com',
  messagingSenderId: '242029589004'
}

// Initialize the Firebase connection
const app = Firebase.initializeApp(config)
const db = app.database()

export default {
  db
}
