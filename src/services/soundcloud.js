import _ from 'lodash'
import sc from 'soundcloud'
import SoundCloudAudio from 'soundcloud-audio'
import axios from 'axios'

const clientID = 'hHfp5B3UvxVFWWBGlXaeGNoWR0ULJz6F'

// Initialize the SoundCloud
sc.initialize({
  client_id: clientID
})
const scPlayer = new SoundCloudAudio(clientID)

export default {
  search (query) {
    return sc.get('/tracks', {
      q: query
    })
    .then(this.cleanTracks)
    .then(this.addCustomProps)
  },
  cleanTracks (tracks) {
    return tracks.map(track => {
      track = _.pick(track, [
        'artwork_url',
        'stream_url',
        'duration',
        'user',
        'title',
        'id',
        'streamUrl'
      ])
      return _.mapKeys(track, (v, k) => _.camelCase(k))
    })
  },
  addCustomProps (tracks) {
    return tracks.map(track => {
      return _.merge(track, {
        acceptedIds: [],
        nextIds: []
      })
    })
  },
  streamSong (Url) {
    scPlayer.play({streamUrl: Url})
  },
  pauseSong () {
    scPlayer.pause()
  },
  playing () {
    return scPlayer.playing
  },
  stopSong () {
    scPlayer.stop()
  },
  getSeconds () {
    return scPlayer.audio.currentTime
  },
  changeTime (seconds) {
    scPlayer.setTime(seconds)
  },
  changeVolume (volume) {
    scPlayer.setVolume(volume)
  },
  loadSong (url) {
    scPlayer.preload(url)
  },
  getTopSongsOfGenre (genre, number) {
    return axios.get(`https://crossorigin.me/https://api-v2.soundcloud.com/charts?kind=top&genre=soundcloud%3Agenres%3A${genre}&client_id=${clientID}&limit=${number}&linked_partitioning=1`)
    .then(function (response) {
      return response.data.collection.map(function (obj) {
        obj.track.streamUrl = `${obj.track.uri}/stream`
        return obj.track
      })
    })
    .then(this.cleanTracks)
    .then(this.addCustomProps)
    .catch(function (error) {
      console.log(error)
    })
  }
}
