import axios from 'axios'

const base = 'api/sms'

function onError (error) {
  console.error(error)
}

export default {
  upNext (room, ids) {
    return axios.post(`${base}/next`, {
      room,
      ids
    }).catch(onError)
  },
  requestAccepted (room, ids) {
    return axios.post(`${base}/accepted`, {
      room,
      ids
    }).catch(onError)
  }
}
