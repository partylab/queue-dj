export default [
  {
    'artworkUrl': 'https://i1.sndcdn.com/artworks-000129101224-nncvns-large.jpg',
    'duration': 245678,
    'id': 223228432,
    'streamUrl': 'https://api.soundcloud.com/tracks/223228432/stream',
    'title': 'It\'s Strange (Ft. K.Flay)',
    'user': {
      'avatar_url': 'https://i1.sndcdn.com/avatars-000269887086-ll45bw-large.jpg',
      'id': 24969134,
      'kind': 'user',
      'last_modified': '2017/02/01 23:51:00 +0000',
      'permalink': 'louisthechild',
      'permalink_url': 'http://soundcloud.com/louisthechild',
      'uri': 'https://api.soundcloud.com/users/24969134',
      'username': 'Louis The Child'
    }
  },
  {
    'artworkUrl': 'https://i1.sndcdn.com/artworks-000142099971-055xlk-large.jpg',
    'duration': 228827,
    'id': 240674505,
    'streamUrl': 'https://api.soundcloud.com/tracks/240674505/stream',
    'title': 'Ty Dolla $ign - Blasé (Louis The Child Remix)',
    'user': {
      'avatar_url': 'https://i1.sndcdn.com/avatars-000269887086-ll45bw-large.jpg',
      'id': 24969134,
      'kind': 'user',
      'last_modified': '2017/02/01 23:51:00 +0000',
      'permalink': 'louisthechild',
      'permalink_url': 'http://soundcloud.com/louisthechild',
      'uri': 'https://api.soundcloud.com/users/24969134',
      'username': 'Louis The Child'
    }
  },
  {
    'artworkUrl': 'https://i1.sndcdn.com/artworks-000105490136-05461d-large.jpg',
    'duration': 186740,
    'id': 189292714,
    'streamUrl': 'https://api.soundcloud.com/tracks/189292714/stream',
    'title': 'SoySauce - Broken Record (feat. Joni Fatora) (Louis The Child Remix)',
    'user': {
      'avatar_url': 'https://i1.sndcdn.com/avatars-000269887086-ll45bw-large.jpg',
      'id': 24969134,
      'kind': 'user',
      'last_modified': '2017/02/01 23:51:00 +0000',
      'permalink': 'louisthechild',
      'permalink_url': 'http://soundcloud.com/louisthechild',
      'uri': 'https://api.soundcloud.com/users/24969134',
      'username': 'Louis The Child'
    }
  },
  {
    'artworkUrl': 'https://i1.sndcdn.com/artworks-000188322328-4xb4ic-large.jpg',
    'duration': 192043,
    'id': 287689887,
    'streamUrl': 'https://api.soundcloud.com/tracks/287689887/stream',
    'title': 'Louis The Child - Fire (Skotia Remix)',
    'user': {
      'avatar_url': 'https://i1.sndcdn.com/avatars-000296149207-p3sr27-large.jpg',
      'id': 148697208,
      'kind': 'user',
      'last_modified': '2017/02/17 12:36:27 +0000',
      'permalink': 'trapitremixes',
      'permalink_url': 'http://soundcloud.com/trapitremixes',
      'uri': 'https://api.soundcloud.com/users/148697208',
      'username': 'Trap It! Remixes'
    }
  },
  {
    'artworkUrl': 'https://i1.sndcdn.com/artworks-000164662532-4ct5ra-large.jpg',
    'duration': 304693,
    'id': 266129708,
    'streamUrl': 'https://api.soundcloud.com/tracks/266129708/stream',
    'title': 'No Problem (feat. Lil Wayne & 2 Chainz)',
    'user': {
      'avatar_url': 'https://i1.sndcdn.com/avatars-000035176561-rg0orz-large.jpg',
      'id': 6969243,
      'kind': 'user',
      'last_modified': '2017/01/14 19:45:24 +0000',
      'permalink': 'chancetherapper',
      'permalink_url': 'http://soundcloud.com/chancetherapper',
      'uri': 'https://api.soundcloud.com/users/6969243',
      'username': '\'Chance The Rapper\''
    }
  },
  {
    'artworkUrl': 'https://i1.sndcdn.com/artworks-000117346024-1s3yn6-large.jpg',
    'duration': 198235,
    'id': 182852309,
    'streamUrl': 'https://api.soundcloud.com/tracks/182852309/stream',
    'title': 'Diplo - Revolution (Unlike Pluto Remix) [feat. Faustix & Imanos and Kai]',
    'user': {
      'avatar_url': 'https://i1.sndcdn.com/avatars-000282229806-37l7lo-large.jpg',
      'id': 512774,
      'kind': 'user',
      'last_modified': '2017/02/21 20:33:30 +0000',
      'permalink': 'unlikepluto',
      'permalink_url': 'http://soundcloud.com/unlikepluto',
      'uri': 'https://api.soundcloud.com/users/512774',
      'username': 'Unlike Pluto'
    }
  },
  {
    'duration': 210305,
    'id': 236886485,
    'streamUrl': 'https://api.soundcloud.com/tracks/236886485/stream',
    'title': 'Paper Pools',
    'user': {
      'avatar_url': 'https://i1.sndcdn.com/avatars-000151253243-e5r4qj-large.jpg',
      'id': 159386988,
      'kind': 'user',
      'last_modified': '2017/01/18 22:57:41 +0000',
      'permalink': 'samantha-p-19',
      'permalink_url': 'http://soundcloud.com/samantha-p-19',
      'uri': 'https://api.soundcloud.com/users/159386988',
      'username': 'Samantha Pynchon'
    }
  }
]
