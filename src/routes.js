import VueRouter from 'vue-router'
import App from './App'
import Site from './Site'

let routes = [
  {
    path: '/',
    component: Site
  }, {
    path: '/:room',
    component: App
  }
]

export default new VueRouter({
  mode: 'history',
  routes
})
